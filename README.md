# 20SCOOPS SCRIPTS

- copy "scripts" directory to target project

## SCRIPTS LIST

- hyky.sh (honyakukonyaku)
    - for export language table CSV file project
    - then import to language table for each platform
    - setting value in .sh
        - platform : {js,ios,sketch,android,php}
        - src_type : {csv}
        - src : export source file path or url
        - des : import destination path
        - config : config of all platforms formatting
        - file_temp : temporary json file