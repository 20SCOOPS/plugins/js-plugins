#!/bin/sh

# platform -> iOS,android,laravel,vuejs,sketch
platform=ios

# sorce type -> csv (just csv for now)
src_type=csv

# sorce
src="https://docs.google.com/spreadsheets/d/1aBJlPW9KyL3u8yrGF0FcbjRsmbg5zVcWpfjfqrVxffY/edit#gid=1886330080"

# destination path
# des=$PRODUCT_NAME
des=src

# config of each platform
config=scripts/config.json

# temporary json file from src
file_temp=scripts/temp

#scripts
./scripts/tools konyak $src_type $src $file_temp
./scripts/tools honyak $platform $file_temp $des $config
rm $file_temp